from character import Character
from normal_state import NormalState
from enraged_state import EnragedState
from poisoned_state import PoisonedState


def Player1VsPlayer2():
    player1 = Character()
    player2 = Character()

    print("Initial state of Player 1:")
    print("Actions:", player1.attack(), player1.move())

    print("\nInitial state of Player 2:")
    print("Actions:", player2.attack(), player2.move())

    player1.enrage_character()
    print("\nPlayer 1 is enraged:")
    print("Actions:", player1.attack(), player1.move())

    player2.poison_character()
    print("\nPlayer 2 is poisoned:")
    print("Actions:", player2.attack(), player2.move())

    print("\nPlayer 1 attacks Player 2:")
    player1.attack_character(player2)

    print("\nPlayer 2 attempts to heal:")
    player2.heal_character()

    player2.change_state(NormalState())
    print("\nPlayer 2 is now in normal state:")
    print("Actions:", player2.attack(), player2.move())

    print("\nPlayer 1 attacks Player 2:")
    player1.attack_character(player2)

    print("\nEnd ..")


if __name__ == "__main__":
    player = Character()
    print("Le personnage est créé dans l'état Normal.")
    print("Actions du personnage:")
    print("Attaque:", player.attack())
    print("Déplacement:", player.move())

    player.change_state(EnragedState())
    print("\nLe personnage est enragé.")
    print("Actions du personnage:")
    print("Attaque:", player.attack())
    print("Déplacement:", player.move())

    player.change_state(PoisonedState())
    print("\nLe personnage est empoisonné.")
    print("Actions du personnage:")
    print("Attaque:", player.attack())
    print("Déplacement:", player.move())

    # Nouvelles fonctionnalités
    player.heal_character()

    target_character = Character()
    print("\nAttaque du personnage sur la cible:")
    player.attack_character(target_character)

    player.change_state(NormalState())
    player.enrage_character()
    print("\nAttaque du personnage enragé sur la cible:")
    player.attack_character(target_character)

    player.poison_character()
    print("\nAttaque du personnage empoisonné sur la cible:")
    player.attack_character(target_character)

    print('------------------------------------')
    print(" Player 1  vs  Player 2 ")
    Player1VsPlayer2()




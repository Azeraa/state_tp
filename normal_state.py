from state import State


class NormalState(State):
    def attack(self):
        return "Attaque normale"

    def move(self):
        return "Déplacement normal"

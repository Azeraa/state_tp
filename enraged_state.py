from state import State

class EnragedState(State):
    def attack(self):
        return "Attaque enragée"

    def move(self):
        return "Déplacement enragé"
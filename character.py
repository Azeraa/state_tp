# character.py
from state import State
from normal_state import NormalState
from enraged_state import EnragedState
from poisoned_state import PoisonedState
from random import randint

class Character:
    def __init__(self):
        self.state = NormalState()

    def change_state(self, new_state):
        self.state = new_state

    def attack(self):
        return self.state.attack()

    def move(self):
        return self.state.move()

    def heal_character(self):
        if isinstance(self.state, NormalState):
            print("Personnage guéri.")
        else:
            print("La guérison n'a pas d'effet sur le personnage actuel.")

    def enrage_character(self):
        print("Personnage enragé.")
        self.state = EnragedState()

    def poison_character(self):
        print("Personnage empoisonné.")
        self.state = PoisonedState()

    def attack_character(self, target):
        random_value = randint(0, 99)

        if random_value < 30:
            print("Attaquant devenu enragé.")
            self.enrage_character()
        elif 30 <= random_value < 60:
            print("Cible devenue enragée.")
            target.enrage_character()
        else:
            print("Attaque réussie, pas de changement d'état.")

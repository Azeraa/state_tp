from state import State

class PoisonedState(State):
    def attack(self):
        return "Attaque empoisonnée"

    def move(self):
        return "Déplacement empoisonné"